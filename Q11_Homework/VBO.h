#pragma once

#include <GL\glew.h>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <MiniMath\Core.h>

struct VBOFace
{
	unsigned vertices[3];
	unsigned textureUVs[3];
	unsigned normals[3];
};

class VBO
{
public:

	VBO();											// Default constructor
	~VBO();											// Destructor

	bool LoadFromFile(const std::string &file);		// Load a mesh and send it to OpenGL
	void Unload();									// Release data from OpenGL (VRAM)

	unsigned GetElementsCount();					// Get count of all elements inside VBO
	void Draw();									// Draw VBO

	GLuint VAO = 0;									// VAO handle
	// GLuint VBO_Vertices = 0;						// Handle for buffer with VBO positions data
	// GLuint VBO_UVs = 0;								// Handle for buffer with VBO texture UVs data
	// GLuint VBO_Normals = 0;							// Handle for buffer with VBO normals data
	GLuint VBO_All_Elements;						// Handle for buffer with all VBO data
	std::vector<unsigned> vboHandles;				// Handles for all VBOs

	// Index for each of the Vertex Arrays
	GLuint VBO_PositionsIndex = 0;					// Index for VBO positions attribute array
	GLuint VBO_TextureUVsIndex = 1;					// Index for VBO texture UVs attribute array
	GLuint VBO_NormalsIndex = 2;					// Index for VBO normals attribute array

	unsigned elementsCount;							// Count of all attribute elements for a vertex
	unsigned elementsPerAttribute = 3;				// Number of elements per each attribute (e.g. position x,y,z = 3, texture u,v = 2, normal x,y,z = 3, colour r,g,b,a = 4
	GLuint elementsType = GL_FLOAT;					// Type of elements in attribute
	unsigned elementsStride;						// Stride
};