#pragma once
#include <iostream>

#include "GL/glut.h"

class Timer
{
public:

	Timer()
	{
		previousTime = glutGet(GLUT_ELAPSED_TIME);	// take initial time
		this->tick();	// start updating elapsed time
	}

	~Timer() { }

	// update the timer clock
	float tick()
	{
		currentTime = glutGet(GLUT_ELAPSED_TIME);
		elapsedTime = currentTime - previousTime;
		previousTime = currentTime;
		return elapsedTime;
	}

	// delta time in milliseconds 
	float getElapsedTimeMS()
	{
		return elapsedTime;
	}

	// delta time in seconds
	float getElapsedTimeSeconds()
	{
		return elapsedTime / 1000.f;
	}

	// current time in milliseconds
	float getCurrentTime()
	{
		return currentTime;
	}

private:

	float currentTime, previousTime, elapsedTime;

};
