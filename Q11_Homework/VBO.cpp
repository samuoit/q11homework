#define _CRT_SECURE_NO_WARNINGS
#include "VBO.h"

#define ATTRIBUTE_ELEMENTS_SIZE 3				// Number of elements per attribute
#define CHAR_BUFFER_SIZE 128					// Size of character buffer for file data parsing
#define BUFFER_OFFSET(i) ((float *)0 + (i))		// Offset for interleaved attribute arrays

VBO::VBO(){}

VBO::~VBO(){}

// Load a VBO and send it to OpenGL
bool VBO::LoadFromFile(const std::string &file)
{
	std::ifstream fileData;
	fileData.open(file);													// Open file

	if (!fileData)	return false;											// If file was not successfully opened exit function and return false
	
	char fileDataString[CHAR_BUFFER_SIZE];									// Prepare character array for reading file data

	std::vector<VBOFace> faces;												// Vector collection of VBOFace accepting vertex faces data
	std::vector<vec3> positions, textureUVs, normals;						// Vector collections of vec3 accepting vertex positions, texture UVs, and normals
	std::vector<float> interleaved;											// Vector collection of interleaved VBO elements for each VBO attribute

	while (!fileData.eof())
	{
		fileData.getline(fileDataString, CHAR_BUFFER_SIZE);

		if (std::strstr(fileDataString, "#") != nullptr)					// Filter out the comment lines in .obj file
			continue;
		else if (std::strstr(fileDataString, "v ") != nullptr)				// Take position line of data from the .obj file
		{
			vec3 temp;
			std::sscanf(fileDataString, "v %f %f %f", &temp.x, &temp.y, &temp.z);
			positions.push_back(temp);
		}
		else if (std::strstr(fileDataString, "vt") != nullptr)				// Take texture UVs data from the .obj file
		{
			vec3 temp;
			std::sscanf(fileDataString, "vt %f %f", &temp.x, &temp.y);
			textureUVs.push_back(temp);
		}
		else if (std::strstr(fileDataString, "vn") != nullptr)				// Take normals data from the .obj file
		{
			vec3 temp;
			std::sscanf(fileDataString, "vn %f %f %f", &temp.x, &temp.y, &temp.z);
			normals.push_back(temp);
		}
		else if (std::strstr(fileDataString, "f") != nullptr)				// Take faces data from the .obj file
		{
			VBOFace temp;
			std::sscanf(fileDataString, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textureUVs[0], &temp.normals[0],
				&temp.vertices[1], &temp.textureUVs[1], &temp.normals[1],
				&temp.vertices[2], &temp.textureUVs[2], &temp.normals[2]);
			faces.push_back(temp);
		}
	}

	fileData.close();

	// Take faces data and load it in proper order to appropriate vector collections for VBO
	for (unsigned i = 0; i < faces.size(); i++)
	{
		for (unsigned j = 0; j < 3; j++)
		{
			// Interleaved data: positions, normals, textureUVs (vvvnnntttvvvnnnttt...)
			interleaved.push_back(positions[faces[i].vertices[j] - 1].x);
			interleaved.push_back(positions[faces[i].vertices[j] - 1].y);
			interleaved.push_back(positions[faces[i].vertices[j] - 1].z);
			interleaved.push_back(normals[faces[i].normals[j] - 1].x);
			interleaved.push_back(normals[faces[i].normals[j] - 1].y);
			interleaved.push_back(normals[faces[i].normals[j] - 1].z);
			interleaved.push_back(textureUVs[faces[i].textureUVs[j] - 1].x);
			interleaved.push_back(textureUVs[faces[i].textureUVs[j] - 1].y);
			interleaved.push_back(textureUVs[faces[i].textureUVs[j] - 1].z);			
		}
	}

	elementsCount = interleaved.size();

	// Create VAO to manage rendering state of VBO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// Generate, bind, and load VBO buffer with all vertex data
	glGenBuffers(1, &VBO_All_Elements);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_All_Elements);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* elementsCount, &interleaved[0], GL_STATIC_DRAW);

	// Bind VBO data - need 3 arrays for each of the attributes
	glEnableVertexAttribArray(0);	// Positions
	glEnableVertexAttribArray(1);	// Normals
	glEnableVertexAttribArray(2);	// Texture UVs

	int vboAttributes = 3;													// Number of attributes in VBO (3 for positions, normals and texture UVs)
	elementsStride = vboAttributes * elementsPerAttribute * sizeof(float);	// Stride for elements in VBO (3 pos, 3 norm, 3 texUV)
	
	// Interleaved attribute arrays (vertex positions, normals, texture UVs)
	glVertexAttribPointer(0, elementsPerAttribute, GL_FLOAT, GL_FALSE, elementsStride, 0);					// Positions
	glVertexAttribPointer(1, elementsPerAttribute, GL_FLOAT, GL_FALSE, elementsStride, BUFFER_OFFSET(3));	// Normals
	glVertexAttribPointer(2, elementsPerAttribute, GL_FLOAT, GL_FALSE, elementsStride, BUFFER_OFFSET(6));	// Texture UVs
	
	// Cleanup - Unbind vertex arrays object
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Clear vector collections
	positions.clear();
	textureUVs.clear();
	normals.clear();
	faces.clear();

	// If everything up to this point was ok, return true
	return true;
}

// Release VBO data from OpenGL (VRAM)
void VBO::Unload()
{
	glDeleteBuffers(1, &VBO_All_Elements);
	glDeleteVertexArrays(1, &VAO);
	VAO = 0;
}

unsigned VBO::GetElementsCount()
{
	return elementsCount;
}

void VBO::Draw()
{
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, elementsCount);
	glBindVertexArray(0);
}
